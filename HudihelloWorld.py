from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import *

table_name = "departments"
# This needs to be changed to your own path
HudiTargetBucket = "/Users/valr/Downloads/Apache-Hudi-Pyspark-master/HudiPyspark/hudi_data/"
departements_source_path = "./departments.csv"
updated_source_path = "updated_departments.csv"


def get_spark_session(app_name='hudi_app'):
    conf = SparkConf() \
        .set('spark.serializer', 'org.apache.spark.serializer.KryoSerializer') \
        .set('spark.jars.packages', "org.apache.hudi:hudi-spark3.1.2-bundle_2.12:0.10.1,org.apache.spark:spark-avro_2.12:3.1.2") \
        .set('spark.port.maxRetries', '40')\
        .set("spark.executor.instances", "4")\
        .set("spark.executor.cores", "2")
    return SparkSession.builder.appName(app_name) \
        .config(conf=conf).enableHiveSupport().getOrCreate()


def inset_into_hudi(session):
    df = session.read.format("csv").option("header", "true")\
        .load(departements_source_path)
    print("============= inset_into_hudi Department Data =============")
    df.show()
    print("=="*100)
    df.write.format("hudi") \
        .option("hoodie.upsert.shuffle.parallelism", 2) \
        .option("hoodie.insert.shuffle.parallelism", 2) \
        .option("hoodie.datasource.write.operation", "upsert") \
        .option("hoodie.datasource.write.precombine.field", "dept_no") \
        .option("hoodie.datasource.write.recordkey.field", "dept_no") \
        .option("hoodie.datasource.write.recordkey.field", "date_add") \
        .option("hoodie.table.name", table_name) \
        .option("hoodie.datasource.write.partitionpath.field", "dept_no") \
        .option("hoodie.datasource.hive_sync.table", table_name) \
        .mode("overwrite") \
        .save(HudiTargetBucket)


def update_data(session):
    print("============= update_data Department Data =============")
    df = session.read.format("csv").option("header", "true") \
        .load(updated_source_path)
    df.show()
    print("==" * 100)
    df.write.format("hudi") \
        .option("hoodie.upsert.shuffle.parallelism", 2) \
        .option("hoodie.insert.shuffle.parallelism", 2) \
        .option("hoodie.datasource.write.operation", "upsert") \
        .option("hoodie.datasource.write.precombine.field", "dept_no") \
        .option("hoodie.datasource.write.recordkey.field", "dept_no") \
        .option("hoodie.datasource.write.recordkey.field", "date_add") \
        .option("hoodie.datasource.write.partitionpath.field", "dept_no") \
        .option("hoodie.table.name", table_name) \
        .option("hoodie.datasource.hive_sync.table", table_name) \
        .mode("append") \
        .save(HudiTargetBucket)


def query_last_snapshot(session):
    dept_view_name = "query_inserted_data_view"
    session.read.format("hudi").load(HudiTargetBucket + "*").createOrReplaceTempView(dept_view_name)

    selected_df = session.sql("select * from {}".format(dept_view_name))
    selected_df.show()


def get_prev_snapshot(session):
    dept_view_name = "query_inserted_data_view"
    session.read.format("hudi").load(HudiTargetBucket).createOrReplaceTempView(dept_view_name)

    # session.sql("select * from {}".format(dept_view_name)).show()

    df = session.sql("select distinct(_hoodie_commit_time) as commitTime from {} order by commitTime".format(dept_view_name))
    # df.show()

    commits = list(map(lambda row: row[0], df.limit(3).collect()))
    end_time = commits[len(commits) - 2]  # get previous snapshot

    # incrementally query data
    incremental_read_options = {
        'hoodie.datasource.query.type':             'incremental',
        'hoodie.datasource.read.begin.instanttime': "000",
        'hoodie.datasource.read.end.instanttime': end_time,
    }

    session.read.format("hudi"). \
        options(**incremental_read_options). \
        load(HudiTargetBucket). \
        createOrReplaceTempView(dept_view_name)

    selected_df = session.sql("select * from {}".format(dept_view_name))
    selected_df.show()


def get_incremental_snapshot(session):
    view_name = "query_inserted_data_view"
    session.read.format("hudi").load(HudiTargetBucket).createOrReplaceTempView(view_name)

    df = session.sql("select distinct(_hoodie_commit_time) as commitTime from {} order by commitTime".format(view_name))
    commits = list(map(lambda row: row[0], df.limit(3).collect()))
    start_time = commits[len(commits) - 2]  # get previous snapshot

    # incrementally query data
    incremental_read_options = {
        'hoodie.datasource.query.type':             'incremental',
        'hoodie.datasource.read.begin.instanttime': start_time,
    }

    session.read.format("hudi"). \
        options(**incremental_read_options). \
        load(HudiTargetBucket). \
        createOrReplaceTempView(view_name)

    selected_df = session.sql("select * from {}".format(view_name))
    selected_df.show()


if __name__ == '__main__':
    spark_session = get_spark_session()

    inset_into_hudi(spark_session)
    print("**" * 200)
    query_last_snapshot(spark_session)
    print("**" * 200)
    update_data(spark_session)
    print("**" * 200)
    get_prev_snapshot(spark_session)
    print("**" * 200)
    query_last_snapshot(spark_session)
    print("**" * 200)
    get_incremental_snapshot(spark_session)

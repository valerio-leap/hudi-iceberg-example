from pyspark import SparkConf
from pyspark.sql import SparkSession, functions as f

namespace = "default"
table = "table"
iceberg_table = f"{namespace}.{table}"

departements_source_path = "./departments.csv"
updated_source_path = "./updated_departments.csv"


def get_spark_session(app_name='iceberg_app'):
    conf = SparkConf() \
        .set('spark.jars.packages', "org.apache.iceberg:iceberg-spark3-runtime:0.13.1") \
        .set('spark.sql.extensions', "org.apache.iceberg.spark.extensions.IcebergSparkSessionExtensions") \
        .set('spark.sql.catalog.iceberg_catalog', "org.apache.iceberg.spark.SparkCatalog") \
        .set('spark.sql.catalog.iceberg_catalog.type', "hadoop") \
        .set("spark.sql.defaultCatalog", "iceberg_catalog") \
        .set("spark.sql.catalog.iceberg_catalog.warehouse", "/Users/valr/Downloads/Apache-Hudi-Pyspark-master/HudiPyspark/iceberg_warehouse") \
        .set('spark.port.maxRetries', '40')\
        .set("spark.executor.instances", "4")\
        .set("spark.executor.cores", "2")
    return SparkSession.builder.appName(app_name) \
        .config(conf=conf).enableHiveSupport().getOrCreate()


def inset_data(session):
    print("============= inset into Department Data =============")
    df = session.read.format("csv").option("header", "true").load(departements_source_path)
    df.createOrReplaceTempView("temp_view")

    spark_session.sql(f"CREATE DATABASE IF NOT EXISTS {namespace}")
    session.sql(f"CREATE OR REPLACE TABLE {iceberg_table} USING iceberg PARTITIONED BY (dept_no) AS select * from temp_view")


def table_exist(session, my_table):
    return session.sql(f"show tables in {namespace}").filter(f.col("tableName") == my_table).count() == 1


def update_data(session):
    print("============= update_data Department Data =============")
    df = session.read.format("csv").option("header", "true").load(updated_source_path)
    # df.createOrReplaceTempView("temp_view")

    df.writeTo(iceberg_table).append()
    # Iceberg does not suppprt MERGE INTO and incremental reads
    # session.sql(f"MERGE INTO {iceberg_table} t USING temp_view v ON t.meter_id = v.meter_id AND t.date_add = v.date_add "
    #             f"WHEN MATCHED THEN UPDATE SET * WHEN NOT MATCHED THEN INSERT *")


def query_last_snapshot(session):
    session.table(iceberg_table).show()
    # session.sql(f"-- SELECT * FROM {iceberg_table}").show()
    # session.read.format("iceberg").load(iceberg_table).show()


def get_prev_snapshot(session):
    session.sql(f"SELECT * FROM {iceberg_table}.files").show()

    df = session.sql(f"SELECT snapshot_id FROM {iceberg_table}.history ORDER BY made_current_at")
    session.sql(f"SELECT * FROM {iceberg_table}.history ORDER BY made_current_at").show(truncate=False)

    snapshots = list(map(lambda row: row[0], df.limit(3).collect()))
    prev_snapshot_id = snapshots[-2]  # get second last snapshot
    # print(prev_snapshot_id)

    session.read\
        .option("snapshot-id", prev_snapshot_id)\
        .format("iceberg")\
        .load(iceberg_table)\
        .show()


def get_incremental_snapshot(session):
    session.sql(f"SELECT * FROM {iceberg_table}.files").show()

    df = session.sql(f"SELECT snapshot_id FROM {iceberg_table}.history ORDER BY made_current_at")
    session.sql(f"SELECT * FROM {iceberg_table}.history ORDER BY made_current_at").show(truncate=False)

    snapshots = list(map(lambda row: row[0], df.limit(3).collect()))
    prev_snapshot_id = snapshots[-2]  # get second last snapshot
    curr_snapshot_id = snapshots[-1]  # get second last snapshot

    session.read\
        .option("start-snapshot-id", prev_snapshot_id)\
        .option("end-snapshot-id", curr_snapshot_id)\
        .format("iceberg")\
        .load(iceberg_table)\
        .show()


if __name__ == '__main__':
    spark_session = get_spark_session()

    inset_data(spark_session)
    query_last_snapshot(spark_session)
    print("**" * 200)
    update_data(spark_session)
    query_last_snapshot(spark_session)
    print("**" * 200)
    get_prev_snapshot(spark_session)
    print("**" * 200)
    get_incremental_snapshot(spark_session)
